import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Contacts from '../../components/Contacts';
import Contact from '../../components/Contact';

const { Navigator, Screen } = createStackNavigator();

const ContactsNavigation = () => {
  return (
    <Navigator>
      <Screen
        name="Contact"
        component={Contacts}
        initialRouteName
        options={{ title: '' }}
      />
      <Screen name="Contacts" component={Contact} />
    </Navigator>
  );
};

export default ContactsNavigation;
