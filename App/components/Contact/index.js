/* eslint-disable curly */
/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import ConfirmDelete from '../ConfirmDelete';
import { useSelector, useDispatch } from 'react-redux';
import {
  Flex,
  HStack,
  Text,
  Image,
  Input,
  Button,
  IconButton,
  Icon as IconElement,
} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

import {
  updateContact,
  deleteContact,
} from '../../common/actions/contactActions';

export const Contact = ({ navigation: { navigate } }) => {
  const dispatch = useDispatch();
  const { id, name, number, photo } = useSelector(
    ({ contacts: { edit } }) => edit
  );
  const addContact = useSelector(
    ({ contacts: { addContact: createContact } }) => createContact
  );
  const [isEdit, setEdit] = useState(addContact);
  const [getName, setName] = useState(name);
  const [getNumber, setNumber] = useState(number);
  const [isConfirmDelete, setConfirmDelete] = useState(false);

  const nameHandle = ({ target: { value } }) => {
    setName(value);
  };

  const numberHandle = ({ target: { value } }) => {
    setNumber(value);
  };

  const editHandle = () => {
    setName(name);
    setNumber(number);
    setEdit(true);
  };

  const cancelHandle = () => {
    setName(name);
    setNumber(number);
    setEdit(false);
  };

  const updateContactHandle = () => {
    if (!getName || !getNumber) return;
    dispatch(updateContact({ id, photo, name: getName, number: getNumber }));
    navigate('Contact');
  };

  const deleteContactHandle = () => {
    deleteContact(id);
    navigate('Contact');
  };

  return isConfirmDelete ? (
    <ConfirmDelete
      isConfirmDelete={isConfirmDelete}
      setConfirmDelete={setConfirmDelete}
      deleteContactHandle={deleteContactHandle}
    />
  ) : (
    <>
      <Flex
        style={{ overflow: 'hidden' }}
        flex={1}
        justify="center"
        align="center">
        {photo ? (
          <Image
            height={300}
            resizeMode="cover"
            source={{
              uri: photo,
            }}
            alt="User photo."
          />
        ) : (
          <Icon name="user" size={300} color="#222" resizeMode="cover" />
        )}
      </Flex>
      <Flex flex={1.3}>
        <Flex
          space={3}
          mb={2}
          mx={4}
          flexDirection="row"
          justifyContent="flex-end"
          alignItems="center">
          <IconButton
            onPress={editHandle}
            icon={
              <IconElement
                as={Icon}
                name="pencil"
                size={30}
                color="#333"
                bg="#eee"
              />
            }
          />
          <IconButton
            onPress={() => {
              setConfirmDelete(true);
            }}
            icon={
              <IconElement
                as={Icon}
                name="trash"
                size={30}
                color="#333"
                bg="#eee"
              />
            }
          />
        </Flex>
        <HStack space={3} mx={4} alignItems="center">
          <IconElement as={<Icon name="user" />} size={30} mr={4} my={4} />
          {isEdit ? (
            <Input
              w="85%"
              size="2xl"
              mb={2}
              pr={0}
              value={getName}
              onChange={nameHandle}
              placeholder="User name."
            />
          ) : (
            <Text fontSize="3xl">{name}</Text>
          )}
        </HStack>
        <HStack space={3} mx={4} alignItems="center">
          <IconElement as={<Icon name="phone" />} size={30} mr={4} my={4} />
          {isEdit ? (
            <Input
              w="85%"
              size="2xl"
              mb={2}
              pr={0}
              value={getNumber}
              onChange={numberHandle}
              placeholder="Phone number."
            />
          ) : (
            <Text fontSize="3xl">{number}</Text>
          )}
        </HStack>
        <HStack space={3} mx={4} alignItems="center">
          {isEdit && (
            <>
              <Button size="lg" bg="#eee" mx={4} onPress={cancelHandle}>
                Cancel
              </Button>
              <Button size="lg" bg="#ddd" onPress={updateContactHandle}>
                Update
              </Button>
            </>
          )}
        </HStack>
      </Flex>
    </>
  );
};

export default Contact;
