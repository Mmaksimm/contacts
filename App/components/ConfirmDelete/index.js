import React from 'react';
import { AlertDialog, Button, Center } from 'native-base';

const ConfirmDelete = ({
  deleteContactHandle,
  isConfirmDelete,
  setConfirmDelete,
}) => {
  const cancelRef = React.useRef();
  return (
    <Center>
      <AlertDialog
        leastDestructiveRef={cancelRef}
        isOpen={isConfirmDelete}
        onClose={() => setConfirmDelete(false)}
        motionPreset={'fade'}>
        <AlertDialog.Content>
          <AlertDialog.Header fontSize="lg" fontWeight="bold">
            Delete contact
          </AlertDialog.Header>
          <AlertDialog.Body>
            Are you sure? You can't undo this action afterwards.
          </AlertDialog.Body>
          <AlertDialog.Footer>
            <Button
              ref={cancelRef}
              style={{ backgroundColor: '#fff' }}
              onPress={() => setConfirmDelete(false)}>
              Cancel
            </Button>
            <Button colorScheme="#ddd" onPress={deleteContactHandle} ml={3}>
              Delete
            </Button>
          </AlertDialog.Footer>
        </AlertDialog.Content>
      </AlertDialog>
    </Center>
  );
};

export default ConfirmDelete;
