/* eslint-disable curly */
/* eslint-disable no-bitwise */
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { StyleSheet, TouchableOpacity } from 'react-native';
import {
  HStack,
  Avatar,
  Text,
  Image,
  Input,
  IconButton,
  Icon as IconElement,
} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

import { getContact, addContact } from '../../common/actions/contactActions';

const Contacts = ({ navigation: { navigate } }) => {
  const dispatch = useDispatch();
  const contacts = useSelector(
    ({ contacts: { contacts: contactList } }) => contactList
  );
  const [getSearching, setSearching] = useState('');

  const searchHandle = ({ target: { value } }) => {
    setSearching(value);
  };

  const getContactHandle = edit => {
    dispatch(getContact(edit));
    navigate('Contacts');
  };

  const addContactHandle = edit => {
    dispatch(addContact(edit));
    navigate('Contacts');
  };

  return (
    <>
      <HStack space={3} mb={2} mx={4}>
        <Input
          w="90%"
          size="2xl"
          mb={2}
          pr={0}
          value={getSearching}
          onChange={searchHandle}
          placeholder="Contacts search..."
          InputLeftElement={
            <IconElement as={<Icon name="search" />} size={25} mx={2} />
          }
        />
        <IconButton
          icon={
            <IconElement
              as={Icon}
              name="plus"
              size={30}
              p={0}
              m={0}
              color="#333"
              bg="#eee"
              onPress={addContactHandle}
            />
          }
          px={2}
        />
      </HStack>

      {contacts
        .filter(({ name }) => {
          const searching = getSearching?.trim();
          if (!searching) return true;
          if (~name.indexOf(searching)) return true;
          return false;
        })
        .sort((us1, us2) => us1 < us2)
        .map(contact => (
          <TouchableOpacity
            key={contact.id}
            onPress={() => {
              getContactHandle(contact);
            }}>
            <HStack space={3} my={2} ml={5} mr={2}>
              <Avatar size="md" bg="#eee" style={styles.avatar}>
                {contact.userPhoto ? (
                  <Image
                    source={{
                      uri: contact.photo,
                    }}
                    alt="User photo"
                    size="xs"
                  />
                ) : (
                  <Icon name="user" size={30} color="#222" />
                )}
              </Avatar>
              <Text fontSize="3xl">{contact.name}</Text>
            </HStack>
          </TouchableOpacity>
        ))}
    </>
  );
};

export default Contacts;

const styles = StyleSheet.create({
  avatar: {
    overflow: 'hidden',
  },
});
