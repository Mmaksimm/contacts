import { createAction } from '@reduxjs/toolkit';

import * as ActionType from './contactActions.Types';

export const getContact = createAction(ActionType.GET_CONTACT);

export const updateContact = createAction(ActionType.UPDATE_CONTACT);

export const addContact = createAction(ActionType.ADD_CONTACT);

export const deleteContact = createAction(ActionType.DELETE_CONTACT);
