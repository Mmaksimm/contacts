import { configureStore } from '@reduxjs/toolkit';

import contactsReducer from './redusers/contactReducer';

const store = configureStore({
  reducer: {
    contacts: contactsReducer,
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware({
      thunk: {
        extraArgument: {},
      },
    });
  },
});

export default store;
