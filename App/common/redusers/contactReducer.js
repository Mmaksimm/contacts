/* eslint-disable prettier/prettier */
import { createReducer } from '@reduxjs/toolkit';
import uuid from 'react-uuid';

import { getContact, updateContact, addContact, deleteContact } from '../actions/contactActions';

const data = [];

for (let i = 100; i; i--) {
  data.push({
    id: uuid(),
    name: `user ${i}`,
    photo: '',
    number: '+38-050-670-31-56',
  });
}

const initialState = {
  contacts: [...data],
  edit: {
    id: 0,
    name: '',
    number: '',
    photo: '',
  },
  delete: {
    id: 0,
    name: '',
    number: '',
    photo: '',


  },
  addContact: false,
};

const contactReducer = createReducer(initialState, builder => {
  builder.addCase(getContact, (state, { payload }) => {
    state.edit = { ...payload };
  });

  builder.addCase(updateContact, (state, { payload }) => {
    state.contacts = state.contacts.map(contact => (
      contact.id !== payload.id
        ? contact
        : payload
    ));
    state.edit.id = '';
    state.addContact = false;
  });

  builder.addCase(addContact, (state, { payload }) => {
    state.edit = {
      id: uuid(),
      name: '',
      number: '',
      photo: '',
    };
    state.addContact = true;
  });

  builder.addCase(deleteContact, (state, { payload }) => {
    state.contacts = state.contacts.filter(contact => (
      contact.id !== payload.id
    ));
    state.edit.id = '';
    state.addContact = false;
  });
});

export default contactReducer;
