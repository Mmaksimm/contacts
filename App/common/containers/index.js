import 'react-native-gesture-handler';
import React from 'react';

import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { NativeBaseProvider, VStack } from 'native-base';
import store from '../store';
import ContactsNavigation from '../../containers/ContactsNavigation';

export default function App() {
  return (
    <NavigationContainer>
      <Provider store={store}>
        <NativeBaseProvider>
          <VStack flex={1} bg="#fff">
            <ContactsNavigation />
          </VStack>
        </NativeBaseProvider>
      </Provider>
    </NavigationContainer>
  );
}
